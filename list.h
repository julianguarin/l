#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include <common.h>
struct List
{
    unsigned int lSize;
    node *head;
    node *tail;
    typedef enum ListPosition{First,Last} ListPosition;

};
typedef struct List*_List;

/**
 * @brief      Create List
 *
 * @return     On success returns a pointer to memory. On failure check errno. 
 */
_List			List();

void			destroy_List(_List l);


void 			*pop(_List l, List::ListPosition pos);
void 			*pop(_List l, void * target);
void			*pop(_List l, int index, bool peek = false);
void 			*pop(_List l);
/**
 * @brief      Push an object into queue.
 *
 * @param[in]  l          The list pointer
 * @param[in]  object       The object pointer
 * @param[in]  freeFunc   An optional callback to freed memory when flushList is invoked. If no freed memory is needed for the object the set this parameter to NULL, but you are responsible for releasing object's memory.
 *
 * @return     On success 0, on error -1
 */
int				listInsert(_List l, void * object, int index, void(freeFunc)(void*)=NULL);
int 			listAppend(_List l, void * object, void(freeFunc)(void*)=NULL);


/**
 * @brief      Empties the lueue. If a freeFunc was provided on 
 *
 * @param[in]  l     The luarter
 */
void			listFlush(_List l);
unsigned int	listCount(_List l);



#endif /* LIST_H_INCLUDED */

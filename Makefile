AR=ar
CC=gcc
CXX=g++
CFLAGS=-c -Wall -I..
LFLAGS=crv
LDFLAGS=-lm
SOURCES=list.cpp
OBJECTS=$(SOURCES:.cpp=.o)
LIBDIR=lib
OUTPUT=$(LIBDIR)/liblist.a
MKDIR=mkdir
.phony:all

all:$(OUTPUT)

$(OUTPUT):$(OBJECTS) $(LIBDIR)
	$(AR) $(LFLAGS) $(OUTPUT) $(OBJECTS)

$(LIBDIR):
	$(MKDIR) $(LIBDIR)


%.o:%.cpp
	$(CXX) $(CFLAGS) $<

clean: 
	rm -rf *.o $(LIBDIR)/*.a
	rm -rf $(LIBDIR) 


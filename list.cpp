
#include <stdlib.h>
#include "list.h"


void init(_List l)
{
	if(!l)return;
	l->lSize = 0;
	l->head = l->tail = NULL;
}
_List List(){

	_List l = (_List) malloc(sizeof(struct List));
	init(l);
	return l;	
}	
void destroy_List(_List l){
	if (listCount(l))listFlush(l);
	free(l);
}
void *pop(_List l, List::ListPosition pos){
	switch(pos){
		case List::First:
			return pop(l,0);
		case List::Last:
			if (listCount(l))
				return pop(l,listCount(l)-1);
	}
	return NULL;
}
void *pop(_List l, void * target){
	node * aux , * last;
	aux = l -> head;
	last = 0x00;
	void * ret = NULL;
	while(aux){
		if (aux->data == target){
			ret = aux->data;
			if (last == 0x00){
				/* First one */
				l->head = l -> head -> next;
				if (!l->head) l->tail=0x00;
				break;
			} else if (!aux->next) {
				/* Last One */
				l->tail = last;
				l->tail -> next = NULL;
				break;
			} else {
				/* In the middle */
				last->next = aux -> next;
				break;
			}
        }
        else {
            last = aux;
            aux = aux -> next;
        }
	}
	if (aux) {
		l -> lSize --;
		free(aux);
	}

	return ret;		
}
void *pop(_List l, int index, bool peek){
	
	void * ret = NULL;
	node * aux = l -> head;
	node * last = NULL;
	if (index<0) index+=listCount(l);

	if (index >= l -> lSize) return NULL;
	while (index--){
		last = aux;
		aux = aux -> next;

	};
	if (last == 0x00 && !peek){
		/* First one */
		l->head = l -> head -> next;
		if (!l->head) l->tail=0x00;
	} else if (!aux->next && !peek) {
		/* Last One */
		l->tail = last;
		last -> next = NULL;
	} else if (!peek){
		/* In the middle */
		last->next = aux -> next;
	}

	ret = aux -> data;
    if (!peek){
        l->lSize--;
        free(aux);
    }
	return ret;
    
}
void *pop(_List l){
	return pop(l,listCount(l)-1);
}

int insert(_List l, void *data, int index, void (freeFunc)(void*)){
	if (!data) return -1;
	if (!listCount(l)) index=0;
	if (index<0) index+=listCount(l);
	
	node * newNode = (node*)malloc(sizeof(node));
	if(!newNode) return -1;
	newNode -> data = data;
	newNode -> next = NULL;
	newNode -> freeFunc = freeFunc;

	node *h,*last;
	for (last = NULL, h = l->head; index >=0; index--, h = h->next) last = h;
	if(!last){
		/* First One */
		newNode -> next = l -> head;
		l -> head = newNode;
	} else if (!h) {
		l -> tail = l -> tail -> next = newNode;
	} else {
		last -> next = newNode;
		newNode -> next = h; 
	}
	l->lSize++;

	return 1; 

}
int listAppend(_List l, void *data, void(freeFunc)(void*))
{	
	if(!data)return -1;
	node *newNode = (node *)malloc(sizeof(node));
	if(newNode == NULL) return -1;
	newNode->data = data;
	newNode->next = NULL;
	newNode->freeFunc = freeFunc;
	

	if(!l->head)l->head=l->tail=newNode;
	else {
		l->tail->next = newNode;
		l->tail = newNode;
	}
	l->lSize++;
	return 0;
}



void listFlush(_List l)
{
	  
	node *temp;
	while(--l->lSize)
	{	
		if (l->head->freeFunc){
			l->head->freeFunc(l->head->data);
		}
		temp = l->head;
		l->head=l->head->next;
		free(temp);
	}
	l->head = l->tail = NULL;
}

unsigned int listCount(_List l)
{
	return l->lSize;
}
